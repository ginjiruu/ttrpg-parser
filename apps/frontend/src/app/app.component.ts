import { Component } from "@angular/core";

@Component({
  selector: "ttrpg-parser-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "frontend";
}

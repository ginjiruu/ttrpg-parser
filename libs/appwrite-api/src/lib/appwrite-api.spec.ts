import { appwriteApi } from './appwrite-api';

describe('appwriteApi', () => {
  it('should work', () => {
    expect(appwriteApi()).toEqual('appwrite-api');
  });
});

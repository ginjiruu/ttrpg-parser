import { Account, Client, Databases, Functions, Storage } from "appwrite";
import { Environment } from "@ttrpg-parser/environment";

export class AppwriteApi {
  private static sdk: Client | null;
  private static activedb: Databases | null;
  private static activeAccount: Account | null;
  private static activeFunctions: Functions | null;

  static provider() {
    if (this.sdk) {
      return this.sdk;
    }

    const client = new Client();

    client.setEndpoint(Environment.APP_ENDPOINT).setProject(
      Environment.APP_PROJECT,
    );

    this.sdk = client;
    return this.sdk;
  }

  static database() {
    if (this.activedb) {
      return this.activedb;
    }
    const db = new Databases(this.provider());
    this.activedb = db;

    return this.activedb;
  }

  static account() {
    if (this.activeAccount) {
      return this.activeAccount;
    }

    const account = new Account(this.provider());
    this.activeAccount = account;

    return this.activeAccount;
  }

  static functions() {
    if (this.activeFunctions) {
      return this.activeFunctions;
    }
    const functions = new Functions(this.provider());
    this.activeFunctions = functions;
    return this.activeFunctions;
  }
}

import { Models } from "appwrite";

export function dataTypes(): string {
  return "data-types";
}

// Character
export interface Character {
  name: string;
}

export type CharacterEntity = Models.Document & Character;

// Player
export interface Player {
  name: string;
}

export type PlayerEntity = Models.Document & Player;
